var modelsModule = require('./models.js');

module.exports = function(args) {
	return new Promise(function(resolve,reject){
		var ret = modelsModule.main_function(args);
		ret.then(function(ret) {
			resolve(JSON.stringify(ret, null, 3));
		});
	});
	
}
