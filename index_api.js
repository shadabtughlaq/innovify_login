var ec_constants = require('./constants.js');
var express = require(ec_constants.node_modules_path+'express');
var http = require(ec_constants.node_modules_path+"http"); 
var app = express();

var httpServer = http.createServer(app);
httpServer.listen(8088);

// on the request to root (localhost:8081/)
app.get('/innovify_login', function (req, res) {
	res.header('Content-type', 'text/html');
	if(req.method == 'GET') {
		var url = require('url');
		var url_parts = url.parse(req.url,true);
		var modelsModule = require('./models.js');
		var ret = modelsModule.main_function(url_parts.query);
		ret.then(function(ret) {
				res.send(JSON.stringify(ret, null, 3));
		});
	}
	else {
		res.send(JSON.stringify({"status":false,"error":"Incorrect Method"}, null, 3));
	}
});

app.use(function(req, res, next) {
    res.status(404).send(JSON.stringify({"status":false,"error":"Incorrect Path"}, null, 3));
});
