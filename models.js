var constants = require('./constants.js');
var mysql 	= require(constants.node_modules_path+'mysql');

//Check if all params present
function parse_clean_data(data) {
	keys = ['username','password'];
	present_count = 0;
	for(var each_key = 0;each_key<keys.length;each_key++) {
		if(keys[each_key] in data) {
			present_count = present_count + 1;
		}
	}
	if(present_count < keys.length) {
		return false;
	}
	return true;
}



exports.main_function = function(data) {
	return new Promise(function(resolve,reject){
		var ret_val = {};
		// Chek if all params provided
		clean_flag = parse_clean_data(data);
		
		if(clean_flag == true) {
			var username 	= data['username'];
			var password 	= data['password'];
			
			var con = mysql.createConnection({
			  host: "localhost",
			  user: "root",
			  password: "root",
			  database: "innovify"
			});

			con.connect(function(err) {
			  if (err) throw err;
			  con.query("SELECT * FROM login WHERE username = '"+username+"'", function (err, result) {
				if (err) throw err;
				if(result.length == 0){
					var ret_val = {"status":clean_flag,"error":"User does not exist"}
					resolve(ret_val);	
				} else {
					con.query("SELECT * FROM login WHERE username = '"+username+"' AND password = '"+password+"'", function (err, result) {
					if (err) throw err;
						if(result.length == 0){
							var ret_val = {"status":clean_flag,"error":"Incorrect Password Entered"}
							resolve(ret_val);	
						} else {
							
							var ret_val = {	"status":clean_flag,
											"message":"Welcome "+username+" You have "+result[0]['access']+" level access"};
							resolve(ret_val);	
						}
					
					});
				
				}
				
			  });
			});
			
			
		} else {
			var ret_val = {"status":clean_flag,"error":"Parameters Absent"}
			resolve(ret_val);
		}
	});
}
